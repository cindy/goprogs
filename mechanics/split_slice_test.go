package mechanics

import (
	"reflect"
	"testing"
)

func TestSplitSlice(t *testing.T) {
	for _, tt := range []struct {
		s         []int
		i         int
		wantLeft  []int
		wantRight []int
	}{
		{
			s:         []int{1, 2, 3},
			i:         1,
			wantLeft:  []int{1},
			wantRight: []int{3},
		},
		{
			s:         []int{1, 2, 3, 4, 5, 6},
			i:         2,
			wantLeft:  []int{1, 2},
			wantRight: []int{4, 5, 6},
		},
		{
			s:         []int{1},
			i:         0,
			wantLeft:  nil,
			wantRight: nil,
		},
		{
			s:         nil,
			i:         0,
			wantLeft:  nil,
			wantRight: nil,
		},
		{
			s:         []int{1},
			i:         2,
			wantLeft:  []int{1},
			wantRight: nil,
		},
		{
			s:         []int{1},
			i:         -1,
			wantLeft:  nil,
			wantRight: []int{1},
		},
	} {
		left, right := SplitSlice(tt.s, tt.i)
		t.Logf("Splitting %v by index %d yielded left %v and right %v",
			tt.s, tt.i, left, right,
		)

		if !reflect.DeepEqual(left, tt.wantLeft) {
			t.Errorf("left %v doesn't match desired %v", left, tt.wantLeft)
		}

		if !reflect.DeepEqual(right, tt.wantRight) {
			t.Errorf("right %v doesn't match desired %v", right, tt.wantRight)
		}
	}
}
